from openerp.addons.website_sale.controllers.main import website_sale
import werkzeug
import math
from openerp.addons.website_sale.controllers.main import table_compute, QueryURL
from openerp import SUPERUSER_ID
from openerp import http
from openerp import tools
from openerp.http import request
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
from pydoc import pager

PPG = 20  # Products Per Page
PPR = 4  # Products Per Row


class website_sale(website_sale):

    @http.route()
    def shop(self, page=0, category=None, search='', ppg=False, minvalue=None, maxvalue=None, lowrange=None, highrange=None, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        res = super(website_sale, self).shop(
            page, category, search, context=context, **post)
        url = "/shop"
        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG
        if lowrange == None or highrange == None or lowrange != None or highrange != None:
            domain = []
            product_obj = pool.get('product.template')
            product_count = product_obj.search_count(
                cr, uid, domain, context=context)
            pager = request.website.pager(
                url=url, total=product_count, page=page, scope=7, url_args=post)
            product_ids = product_obj.search(cr, uid, domain, offset=[
            ], order='website_published desc, website_sequence desc', context=context)
            products = product_obj.browse(
                cr, uid, product_ids, context=context)
            product_price = products._product_template_price(products, domain)
            range1 = math.floor(min(product_price.values()))
            range2 = math.floor(max(product_price.values()))
            res.qcontext['lowrange'] = range1
            res.qcontext['highrange'] = range2

            if minvalue:
                post["minvalue"] = minvalue
            if maxvalue:
                post["maxvalue"] = maxvalue

        if minvalue != None or maxvalue != None:
            if category:
                category = pool['product.public.category'].browse(
                    cr, uid, int(category), context=context)
                url = "/shop/category/%s" % slug(category)
                domain += [('public_categ_ids', 'child_of', int(category))]

            product_obj = pool.get('product.template')
            product_count = product_obj.search_count(
                cr, uid, domain, context=context)
            pager = request.website.pager(
                url=url, total=product_count, page=page, scope=7, url_args=post)
            product_ids = product_obj.search(cr, uid, domain, offset=[
            ], order='website_published desc, website_sequence desc', context=context)
            products = product_obj.browse(
                cr, uid, product_ids, context=context)
            product_price = products._product_template_price(products, domain)
            latest_products = []
            for price in product_price:
                if product_price[price] >= float(minvalue) and product_price[price] <= float(maxvalue):
                    latest_products.append(price)

            domain = request.website.sale_product_domain()
            domain += [('id', 'in', latest_products)]
            product_obj = pool.get('product.template')
            product_count = product_obj.search_count(
                cr, uid, domain, context=context)
            pager = request.website.pager(
                url=url, total=product_count, page=page, step=ppg, scope=7, url_args=post)
            product_ids = product_obj.search(cr, uid, domain, limit=ppg, offset=pager[
                                             'offset'], order='website_published desc, website_sequence desc', context=context)
            products = product_obj.browse(
                cr, uid, product_ids, context=context)
            res.qcontext['product_count'] = product_count
            res.qcontext['category'] = category
            res.qcontext['search'] = search
            res.qcontext['pager'] = pager
            res.qcontext['products'] = products
            res.qcontext['minvalue'] = minvalue
            res.qcontext['maxvalue'] = maxvalue
            res.qcontext['lowrange'] = range1
            res.qcontext['highrange'] = range2

            res.qcontext.update(
                {'bins': table_compute().process(products, ppg)})

        return res
