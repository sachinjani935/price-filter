# -*- encoding: utf-8 -*-
from openerp import api, fields, models, _


class website(models.Model):
    _inherit = "website"

    slider_skin = fields.Selection([('default', 'Default'), ('simple', 'Simple'), (
        'flat', 'Flat'), ('morden', 'Morden'), ('nice', 'Nice')], string="Choose Your Slider Skin")


class website_config_settings(models.TransientModel):
    _inherit = 'website.config.settings'

    slider_skin = fields.Selection([('default', 'Default'), ('simple', 'Simple'), ('flat', 'Flat'), (
        'morden', 'Morden'), ('nice', 'Nice')], string="Choose Your Slider Skin", related="website_id.slider_skin")
