var minavlue,maxvalue,fromvalue,tovalue,currancy

$(document).ready(function(){
	
	minavlue = $("input[id='nvlowrange']").val();
	maxvalue = $("input[id='nvhighrange']").val();
	fromvalue = $("input[id='nvminvalue']").val();
	tovalue = $("input[id='nvmaxvalue']").val();
	currancy = $("input[id='nvcurrncy']").val();
	
	if (!fromvalue){fromvalue=minavlue;};
	if (!tovalue){tovalue=maxvalue;};
	
	$("#priceslider").ionRangeSlider({
		min: minavlue,
		max: maxvalue,
		from:fromvalue,
		to:tovalue,
		type:'double',
		bar:'red',
		prefix:currancy,
		onFinish: function(data){
	    	$("input[name='minvalue']").attr("value",data.from);
			$("input[name='maxvalue']").attr("value",data.to);
			$("#priceslider").closest("form").submit();
			
			
		},
	});
	
});