{
    'name': 'Website Price Filter',
    'summary': 'Website Price Filter',
    'description': 'Website Price Filter',
    'category': 'website',
    'version': '9.0.1.0',
    'author': 'Devil Developers',
    'depends': ['website_sale'],
    'data': [
         'views/res_config.xml',
         'views/template.xml',
    ],
    'installable': True,
    'application':True,

}
